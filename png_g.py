"""=================================================================================
#
# Author:	ChA0s
#
# E-mail:	arrogone@gmail.com
#
# Filename:		png_g.py
# Created at: 		2010-04-04 10:06
# Last modified: 	2010-04-04 10:06
#
# Description: Generate PNG format image from given grayscale date(as default)
#
================================================================================="""
import struct,os, zlib, time


class png():

	def __init__(self,data ,fname = "genimg.png",wt =50,ht =60,zoom=5,bt=8,cr=3,flag=0,pdata=''):
		self.z = zoom
		self.w = wt
		self.h = ht
		self.b = bt
		self.c = cr
		self.fname = fname
		self.plte_flag = flag
		self.data = data


	def dump(self):

		b=self.png_sgnt()
		b+=self.png_ihdr(self.w,self.h,self.z)
		#self.png_trns()
		#self.png_srgb()
		#self.png_gama()
		#self.png_phys()
		if self.plte_flag == 1:
			b+=self.png_plte(pdata)
		b+=self.png_idat(self.data)
		b+=self.png_iend()

		return b

#Line filter called before compressing 
	def line_filter(self,data):
		b='0'
		for i in range(0,self.h):
			for j in range(0,self.w):
				k=i*self.w + j
				b+=data[k]
			b+='0'
		return b

#PNG signature at the most front of PNG datastream
	def png_sgnt(self):
		return struct.pack('>HHHH',0x8950,0x4e47,0x0d0a,0x1a0a)

#PNG IHDR chunk. Bit depth set default as 8,color type as 0
	def png_ihdr(self,w,h,z):
		b=struct.pack('>HH',0x0000,0x000d)
		temp=struct.pack('>HH',0x4948,0x4452)
		temp+=struct.pack('!2I',w*z,h*z)		#4byte type
		temp+=struct.pack('>BB',8,0)	#1byte type
		temp+=struct.pack('>3B',0,0,0)
		b+=temp
		h=zlib.crc32(temp)&0xffffffff
		b+=struct.pack('>L',h)
		#b=self.pack_chunk('IHDR',struct.pack("!2I5B",self.w*self.z,self.h*self.z,8,2,0,0,0))
		return b

#PNG IDAT chunk. zlib compress method set as default 0
	def png_idat(self,data):
		#temp+=struct.pack('>HB',0x78da,0x01)		#Alternating LZ777 compressing
		#temp+=struct.pack('>BB',size,(size ^ 0xff))
		#zdata=zlib.adler32('00')
		#temp+=struct.pack('>B',00)
		#for i in range(0,self.h):
		#	base = i
		#	for j in range(0,self.w):
		#		zdata=zlib.adler32(d[base*self.h+j],zdata)
		#		temp+=struct.pack('>s',d[base*self.h+j])
		#	temp+=struct.pack('>B',0)
		#	zdata=zlib.adler32('00',zdata)
		#temp+=struct.pack('>L',zdata)
		"""
		dl=self.line_filter(d)
		zdata=zlib.compress(dl,9)
		size = len(zdata)
		b = struct.pack('>I',size)
		id= struct.pack('>HH',0x4944,0x4154)

		b+= id
		for j in range(0,len(zdata)):
			b+=struct.pack('>s',zdata[j])
		h=zlib.crc32(id+zdata)

		b+=struct.pack('>L',h)
	#	b+= struct.pack('>L',self.crc(temp))
	"""
		raw=[]
		for x in range(self.h):
			for j in range(self.z):

				raw.append(chr(0))
				for y in range(self.w):
					for k in range(self.z):
						raw.append(struct.pack("!s",data[x*self.w+y]))
		raw=''.join(raw)
		b=self.pack_chunk('IDAT',zlib.compress(raw,9))
		return b

#PNG PLTE chunk.
	def png_plte(self,data):
		size=len(data)
		b=struct.pack('>I',size)
		temp = struct.pack('>HH',0x504c,0x5445)
		for i in range(0,size):
			temp+=struct.pack('>s',data[i])
		h=zlib.crc32(temp)
		b+=temp
		b+=struct.pack('>L',h)
		return b

#PNG sRGB chunk
	def png_srgb(self):
		b=struct.pack('>I',1)
		temp=struct.pack('>I',0x73524742)
		temp+=struct.pack('>B',0x00)
		h=zlib.crc32(temp)
		b+=temp
		b+=struct.pack('>L',h)
		return b

#PNG gAMA chunk.
	def png_gama(self):
		b=struct.pack('>I',4)
		temp=struct.pack('>I',0x67414d41)
		temp+=struct.pack('>I',0x0000b18f)
		h=zlib.crc32(temp)
		b+=temp
		b+=struct.pack('>L',h)
		return b

#PNG pHYS chunk.
	def png_phys(self):
		b=struct.pack('>I',9)
		temp=struct.pack('>I',0x70485973)
		temp+=struct.pack('>I',0x00000ec3)
		temp+=struct.pack('>I',0x00000ec3)
		temp+=struct.pack('>B',1)
		h=zlib.crc32(temp)
		b+=temp
		b+=struct.pack('>L',h)
		return b

	def png_trns(self):
		b= self.pack_chunk('tRNS', struct.pack("!6B",0xff,0xff,0xff,0xff,0xff,0xff))
		return b

	def pack_chunk(self, tag, data):
		check = tag + data
		return struct.pack("!I",len(data)) + check+ \
				struct.pack("!I",zlib.crc32(check) & 0xffffffff)

	def readata(self):
		fd=open('000.dat','rb')
		fd.seek(0)
		m=self.w*self.h
		self.data=fd.read(m)
		self.fname='genimg.png'
		self.w=60
		self.h = 40
		self.generate(self.data)


#PNG IEND chunk.	
	def png_iend(self):
		return struct.pack('>HHHHHH',0x0000,0x0000,0x4945,0x4e44,0xae42,0x6082)
"""	my CRC
	def mk_crc_tab(self):
		c=0xffffffffL

		for n in range(0,256L):
			c = n
			for k in range(0,8):
				if (c&1):

					c = 0xedb88320L ^ (c>>1)
				else:
					c = c >> 1
			self.crc_tab.append(c)

		self.crc_tab_f = 1

		return self.crc_tab_f 
	def update_crc(self,data):
		c = 0xffffffff

		if self.crc_tab_f == 0:
			self.mk_crc_tab()
		for n in range(0,len(data)):

			c = self.crc_tab[(c ^ ord(data[n])) & 0xff] ^ (c >> 8)

		return c

	def crc(self, data):
		return self.update_crc(data) ^ 0xffffffffL

"""
if __name__ == "__main__":

#	tm = time.localtime()
#	ts = str(tm[0])+str(tm[1])+str(tm[2])
#	os.mkdir(ts)

	f=open("000.dat",'rb')
	
	w=50
	h=60
	offset=3
	

	sum=len(f.read())/(w*h)
	
	print(sum)
	
#	for i in range(1):
#		f.seek(sum*w*h)
#		fdata=f.read(w*h)
#		
#		img=open(str(i),'wb')
#		wp=png(data=fdata,wt=50,ht=60)
#		img.write(wp.dump())
#		img.close()
#		
#	f.close()

	for i in range(3,sum):
#		f=open("000.dat",'rb')
		seek=(w)*(h)*i-5*w
		f.seek(seek-20)
		dt=f.read(w*h)
		
		dd=open(str(i)+'.'+'png','wb')
		wd=png(data=dt,wt=50,ht=60)
		dd.write(wd.dump())
		dd.close()
	f.close()
