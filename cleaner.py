# Clean all vim temp files *~,and ~.swp

import stat,os

def looplast (lists):
	mode = os.stat(lists)[0]

	if stat.S_ISREG(mode):
		if lists[-1]=='~' or lists[-4:-1]=='.sw':
			os.remove(lists)
			print lists
	else :
		sublist = os.listdir(lists)
		for subf in sublist:
			subf = lists+'\\'+subf
			looplast(subf)
			
dirs = os.getcwd()

looplast(dirs)

input()
