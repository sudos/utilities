#!/usr/bin/python

import wx, serial, threading, sys, warnings, png_g, os, time
from PIL import Image

class Listbox(wx.Frame):
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, size=(880, 500))
        
        baudrate=['9600','19200','38400','115200']
        self.br = '9600'
        databits=['8','7','6','5']
        self.db = '8'
        parity=['even','odd','None','space']
        self.pr = 'None'
        stopbits=['1','1.5','2']
        self.sb = '1'
        flowctrl=['Hardware','None']
        self.fc = 'None'
        vbox = wx.BoxSizer(wx.VERTICAL)

	menubar = wx.MenuBar()
	file = wx.Menu()
	edit = wx.Menu()
	ctrl = wx.Menu()
	about = wx.Menu()
	
	file.Append(101, '&quit', 'Quit')
	edit.Append(101, '&option', 'Option')
	about.Append(101, '&about', 'About Me')

	menubar.Append(file, '&File')
	menubar.Append(edit, '&Edit')
	menubar.Append(ctrl, '&Ctrl')
	menubar.Append(about, '&About')

	self.SetMenuBar(menubar)

        panel = wx.Panel(self, -1,pos=(0,0),size=(480,460),style=wx.SUNKEN_BORDER)
	"""
        
        wx.ComboBox(panel, 3, pos=(10,330),size=(50,50),choices=baudrate,
        style=wx.CB_READONLY)
        wx.ComboBox(panel, 4, pos=(110,330),size=(50,-1),choices=databits,
        style=wx.CB_READONLY)
        wx.ComboBox(panel, 5, pos=(210,330),size=(50,-1),choices=parity,
        style=wx.CB_READONLY)
        wx.ComboBox(panel, 6, pos=(310,330),size=(50,-1),choices=stopbits,
        style=wx.CB_READONLY)
        wx.ComboBox(panel, 7, pos=(410,330),size=(50,-1),choices=flowctrl,
        style=wx.CB_READONLY)
	"""

        hbox = wx.BoxSizer(wx.HORIZONTAL)
        

        self.text = wx.TextCtrl(panel, 3, 
        'Please select the varibles below before pressing Start button to get connected ...............',
		size=(530, 300), style=wx.TE_MULTILINE)

        ppanel=wx.Panel(self,-1,pos=(480,0),size=(400,460),style=wx.SUNKEN_BORDER)
        self.picture=wx.StaticBitmap(ppanel,-1,pos=(0,0),size=(250,300),style=wx.SUNKEN_BORDER)
        self.picture.SetBackgroundColour(wx.WHITE)
        
        hbox.Add(self.text,-1)
        hbox.Add(self.picture,-1)


        btn = wx.Button(panel, wx.ID_CLOSE, 'Close',(100,370))
        st = wx.Button(panel, 2, 'Start',(10,370))
        pause = wx.ToggleButton(panel, 8, 'Pause', (190,370))
        load = wx.Button(ppanel,9,'Load Image',pos=(10,370))
        stop = wx.Button(ppanel,10,'Stop',pos=(120,370))
        genimg = wx.Button(panel, 11, 'GenImg', (290,370))

        vbox.Add(hbox, 0, wx.ALIGN_CENTRE)
		
        panel.SetSizer(vbox)

        wx.EVT_BUTTON(self, wx.ID_CLOSE, self.OnClose)
        wx.EVT_BUTTON(self, 2, self.OnStart)
	"""
        wx.EVT_COMBOBOX(self, 3, self.OnSelect1)
        wx.EVT_COMBOBOX(self, 4, self.OnSelect2)
        wx.EVT_COMBOBOX(self, 5, self.OnSelect3)
        wx.EVT_COMBOBOX(self, 6, self.OnSelect4)
        wx.EVT_COMBOBOX(self, 7, self.OnSelect5)
	"""
        wx.EVT_TOGGLEBUTTON(self, 8, self.OnPause)
        wx.EVT_BUTTON(self, 9, self.OnLoad)
        wx.EVT_BUTTON(self, 10, self.OnStop)
        wx.EVT_BUTTON(self, 11, self.OnGenimg)

	StatusBar=self.CreateStatusBar()
	StatusBar.SetStatusText("Ready...")



        self.Show(True)
        self.Centre()

    def OnClose(self, event):
        #ss=''
        #self.df.write(ss)
        self.df.close();
        sys.exit()
        
    def OnStart(self, event):
    	self.t = threading.Thread(target=self.Brun)
    	self.t.deamon=True
    	self.t.start()

    def OnPause(self, event):
	    if self.status:
		    self.status = 0
		    #sys.exitfunc()

	    else :
		    self.status =1
		    #self.n.join()
		    #self.t.start()

    def OnLoad(self, event):
	path = '2010528'
	cur = os.getcwd()
	lst = os.listdir(cur)
	tf=path in lst
	if tf:
		os.chdir(path)

	for i in range(1,148):
		time.sleep(0.05)
		name = str(i)+'.png'
        	self.picture.SetFocus()
        	self.picture.SetBitmap(wx.Bitmap(name))

    def OnStop(self, event):
        #warnings.simplefilter("ignore",wx)
        self.picture.SetFocus()
        self.picture.SetBitmap(wx.Bitmap('001.png'))

    def OnGenimg(self, event):
	    m=png_g(data=self.data,wt=50,ht=60)
	    md=m.dump()
	    f=open("gen.png",'wb')
	    f.write(md)
	    f.close()

	    """
	    img = Image.open("genimg.png")
	    re_img = img.resize(120)
	    re_img.save("genimg.png")
	    """
	    self.picture.SetFocus()
	    self.picture.SetBitmap(wx.Bitmap('gen.png'))

	    

    def OnSelect1(self, event):
    	self.br = event.GetSelection()

    def OnSelect2(self, event):
    	self.db = event.GetSelection()
    	
    def OnSelect3(self, event):
    	self.pr = event.GetSelection()
    	
    def OnSelect4(self, event):
    	self.sb = event.GetSelection()
    	
    def OnSelect5(self, event):
    	self.fc = event.GetSelection()
    	

    def Brun(self):
#	self.nf=open("name.dat",'w')
        self.df=open("000.dat",'ab')

	self.StatusBar.SetStatusText("About to writing data to 000.dat...")
        self.status = 1
        ser = serial.Serial(port='COM1',baudrate=115200,bytesize=serial.EIGHTBITS,parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,timeout=None,xonxoff=0,rtscts=0)

        ser.baudrate = 115200
        str = '\n'
	self.data=''
	self.count = 0
        while True:
            str = ser.read(ser.inWaiting())
            if len(str):
#		self.data+=str
				self.df.write(str)
                #self.count+=1
		#sbars=str(self.count)+"been written!"
		#self.StatusBar.SetStatusText(s)
               # str=hex(ord(str))
				self.text.AppendText(". ")
            while self.status ==0 :
                self.status = self.status
            

    def Nope(self):
        while True:
            i = 0
            print 'a'
                
app = wx.App()
Listbox(None, -1, 'Python Serial terminal')
app.MainLoop()
